# Aplikasi_Ekspedisi_Perjalanan_Stack
# I.S:
# F.S:

class Node:
    def __init__(self, info):
        self.info = info
        self.next = None
        self.prev = None


class DoubleLinkedList:
    def __init__(self):
        self.top = None

    def Kosong(self):
        return self.top is None

    def Push(self, DataBaru):
        Baru = Node(DataBaru)
        if self.Kosong():
            Baru.next = None
        else:
            Baru.next = self.top
            self.top.prev = Baru
        self.top = Baru

    def TampilDataStack(self):
        print("--------------------------")
        print("Isi Data Jasa Ekspedisi")
        if self.Kosong():
            print("--------------------------")
            print("Data Kosong")
        else:
            bantu = self.top
            while bantu is not None:
                print("--------------------------")
                print('Kode Invoice \t\t: ', bantu.info['Kode Invoice'])
                print('Nama Pengirim \t\t: ', bantu.info['Nama Pengirim'])
                print('Nama Penerima \t\t: ', bantu.info['Nama Penerima'])
                print('Berat Paket \t\t: ', bantu.info['Berat Paket'], ' Kg')
                print('Panjang Paket \t\t: ', bantu.info['Panjang Paket'], ' Kg')
                print('Lebar Paket \t\t: ', bantu.info['Lebar Paket'], ' cm')
                print('Tinggi Paket \t\t: ', bantu.info['Tinggi Paket'], ' cm')
                print('Tarif Perjalanan \t: Rp. ', bantu.info['Tarif Perjalanan'], ",-")
                bantu = bantu.next
            print()

    def Pop(self):
        if self.top is None:
            print("Data Stack Kosong")
        elif self.top.next is None:
            bantu = self.top.info
            self.top = None
            print("Data Terhapus : ", bantu)
        else:
            bantu = self.top.info
            self.top = self.top.next
            self.top.prev = None
            print("Data Terhapus : ", bantu)

# Menu Utama
list1 = DoubleLinkedList()
print("--------- Menu Pilihan ---------")
print("================================")
print("[1] Tambah Data Paket")
print("[2] Hapus Data Paket")
print("[0] Keluar")
print("================================")
menu = int(input("Masukan Pilihan : "))
while (menu != 0):
    # Push data ke Stack
    if (menu == 1):
        kode_invoice = str(input("Masukan Kode Invoice :"))
        nama_pengirim = str(input("Masukan Nama Pengirim :"))
        nama_penerima = str(input("Masukan Nama Penerima :"))
        berat_paket = float(input("Masukan Berat Paket (Kg) : "))
        panjang_paket = float(input("Masukan Panjang Paket (cm) : "))
        lebar_paket = float(input("Masukan Lebar Paket (cm) : "))
        tinggi_paket = float(input("Masukan Tinggi Paket (cm) :"))
        tarif = (berat_paket * 5000) + (panjang_paket * lebar_paket * tinggi_paket * 100)
        print("-----------------------------------------------------")
        print("Tarif Harga : ", tarif)
        print("=====================================================")
        DataBaru = {
            'Kode Invoice': kode_invoice,
            'Nama Pengirim': nama_pengirim,
            'Nama Penerima': nama_penerima,
            'Berat Paket': berat_paket,
            'Panjang Paket': panjang_paket,
            'Lebar Paket': lebar_paket,
            'Tinggi Paket': tinggi_paket,
            'Tarif Perjalanan': tarif
        }
        list1.Push(DataBaru)
        list1.TampilDataStack()
    elif (menu == 2):
        list1.Pop()
    else:
        pass
    print("--------- Menu Pilihan ---------")
    print("================================")
    print("[1] Tambah Data Paket")
    print("[2] Hapus Data Paket")
    print("[0] Keluar")
    print("================================")
    menu = int(input("Masukan Pilihan : "))













